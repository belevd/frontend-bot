import UsersService from "./users.service.js";
import {NotFoundError} from "../config/errors.js";

export default class UsersController {
  constructor (bot) {
    this.bot = bot;

    this.bot.hears('Пользователи', this.showUsers)
    this.bot.hears('Кто я?', this.getUser)
  }

  async showUsers(ctx) {
    const users = await UsersService.getUsers();
    users.forEach((user) => ctx.reply(`Имя: @${user.user.username}\n` + `Роль: ${user.role}`))
  }

  async getUser(ctx) {
    try {
      const user = await UsersService.getUserById(ctx.message.from.id);
      ctx.reply(`Привет, @${user.user.username}\n` + `Ты ${user.role}`)
    } catch (e) {
      if (e instanceof NotFoundError) {
        ctx.reply(`Привет, @${ctx.message.from.username}\n` +
                  'Вас нет в списке особых пользователей')
        return
      }
      Promise.reject(e)
    }
  }

}