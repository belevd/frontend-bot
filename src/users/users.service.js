import User from './users.model.js';
import { NotFoundError } from '../config/errors.js';

export default class UsersService {
  static getUsers() {
    return User.find();
  }

  static async getUserById(id) {
    const user = await User.findOne({ 'user.id': id });
    if (!user) {
      throw new NotFoundError();
    }
    return user;
  }
}
