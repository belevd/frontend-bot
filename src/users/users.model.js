import mongoose from 'mongoose';

export default mongoose.model('User', { user: Object, role: String });