import Link from './link.model.js';
import { NotFoundError } from '../config/errors.js';

export default class LinkService {
  static async addLink({ name, url, author }) {
    const link = new Link({ name, url, likes: 0, likers: [], author });
    return await link.save();
  }

  static getLinks() {
    return Link.find();
  }

  static async getLink(url) {
    const link = await Link.findOne({ url });
    if (!link) {
      throw new NotFoundError();
    }
    return link;
  }

  static removeLinks() {
    return Link.deleteMany({});
  }

  static removeLink(url) {
    return Link.deleteOne({ url });
  }

  static async addLike(url, user_id) {
    const link = await this.getLink(url);
    if (link.likers.includes(user_id)) {
      return link;
    }
    const update = { likes: link.likes + 1, likers: [...link.likers, user_id] };
    await Link.updateOne({ url }, update);
    return {
      ...link,
      ...update,
    };
  }
}
