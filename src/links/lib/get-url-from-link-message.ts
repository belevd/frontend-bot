export default function getUrlFromLinkMessage(text: string): string {
  return text.match(/Ссылка: \S*/gm)[0].split(': ')[1];
}
