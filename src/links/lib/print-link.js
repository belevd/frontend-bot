export default function printLink(link) {
  return `Название: ${link.name}\n` +
          `Ссылка: ${link.url}\n` +
          `Автор: ${link.author.first_name} ${link.author.last_name}`
}