export default function getUrlFromLinkMessage(text) {
  return text.match(/Ссылка: [\S]*/gm)[0].split(': ')[1]
}