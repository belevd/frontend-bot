import printLink from './print-link.js';
import getUrlFromLinkMessage from "./get-url-from-link-message.js";

export { printLink, getUrlFromLinkMessage };
