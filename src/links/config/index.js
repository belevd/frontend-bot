import { Markup } from 'telegraf';
import { MAIN } from '../../config';

export const ADD_LINK = 'Добавить ссылку';
export const SHOW_LINKS = 'Посмотреть ссылки';
export const REMOVE_LINKS = 'Удалить ссылки';
export const LINKS_KEYBOARD = Markup.keyboard([
  [ADD_LINK, SHOW_LINKS, REMOVE_LINKS],
  [MAIN],
]);

export const ADD_LIKE = 'link-like';
