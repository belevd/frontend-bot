import { model, Schema } from 'mongoose';
import { TLink } from './types';

const linkSchema = new Schema<TLink>({
  name: String,
  url: String,
  likes: Number,
  likers: Array,
  author: Object,
});

export default model<TLink>('Link', linkSchema);
