export type Author = {
  id: Boolean;
  first_name: string;
  last_name: string;
};

export class TLink {
  name: string;
  url: string;
  author: Author;
  likes?: number;
  likers?: number[];
}
