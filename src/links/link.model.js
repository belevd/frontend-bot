import mongoose from 'mongoose';

export default mongoose.model('Link', {
  name: String,
  url: String,
  likes: Number,
  likers: Array,
  author: Object,
});
