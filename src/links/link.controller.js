import { Markup, Telegraf } from 'telegraf';
import LinkService from './link.service.js';
import {ADD_LIKE, ADD_LINK, REMOVE_LINKS, SHOW_LINKS} from './config/index.js';
import { getUrlFromLinkMessage, printLink } from './lib/index.js';
import { NotFoundError } from '../config/errors.js';
import {UsersService} from "../users/index.js";

const linkDeleteBtn = Markup.button.callback('Удалить', 'link-delete');

const addLikesToKeyboard = (likes) => {
  return Markup.inlineKeyboard([
    linkDeleteBtn,
    Markup.button.callback(`👍 ${likes}`, ADD_LIKE),
  ]);
};

export default class LinkController {
  constructor(bot) {
    this.bot = bot;
    this.bot.use(
      Telegraf.optional(
        async (ctx) => {
          return ctx.session.addingLink;
        },
        (ctx) => this.addLinkStateMachine(ctx)
      )
    );
    this.bot.action('link-delete', this.removeLink);
    this.bot.action(ADD_LIKE, this.addLike);
    this.bot.hears(ADD_LINK, this.addLink);
    this.bot.hears(SHOW_LINKS, this.getLinks);
    this.bot.hears(REMOVE_LINKS, this.removeLinks);
  }

  async addLink(ctx) {
    ctx.session.addingLink = {
      name: '',
      link: '',
      step: 0,
    };
    ctx.reply('Введите имя ссылки');
  }

  async addLinkStateMachine(ctx) {
    if (ctx.session.addingLink.step === 0) {
      await this.addLinkStep1(ctx);
      return;
    }
    if (ctx.session.addingLink.step === 1) {
      this.addLinkStep2(ctx);
    }
  }

  async addLinkStep1(ctx) {
    ctx.session.addingLink.name = ctx.message.text;
    ctx.session.addingLink.step += 1;
    ctx.reply('А теперь пришли мне саму ссылку');
  }

  async addLinkStep2(ctx) {
    ctx.session.addingLink.url = ctx.message.text;
    const { name, url } = ctx.session.addingLink;
    const link = await LinkService.addLink({
      name,
      url,
      likes: 0,
      author: ctx.message.from,
    });
    ctx.session.addingLink = null;
    await ctx.reply('Ссылка успешно добавлена');
    ctx.reply(printLink(link));
  }

  async getLinks(ctx) {
    const links = await LinkService.getLinks();
    if (links.length === 0) {
      ctx.reply('Сейчас в базе нет ссылок');
      return;
    }
    await ctx.reply('Вот ссылки, которые удалось найти:');
    links.forEach((link) =>
      ctx.reply(printLink(link), addLikesToKeyboard(link.likes))
    );
  }

  async removeLinks(ctx) {
    try {
      const user = await UsersService.getUserById(ctx.message.from.id);
      if (user.role !== 'admin') {
        ctx.reply('У вас нет прав на эту операцию');
        return;
      }
      await LinkService.removeLinks();
      ctx.reply('Все ссылки успешно удалены!');
    } catch (e) {
      ctx.reply('У вас нет прав на эту операцию');
    }
  }

  async removeLink(ctx) {
    try {
      const url = getUrlFromLinkMessage(ctx.update.callback_query.message.text);
      const link = await LinkService.getLink(url);
      const user = await UsersService.getUserById(ctx.update.callback_query.from.id).catch((e) => {
        if (e instanceof NotFoundError) {
          return {}
        }
        Promise.reject(e);
      });
      if (link.author.id === ctx.update.callback_query.from.id || user.role === 'admin') {
        await LinkService.removeLink(url);
        ctx.answerCbQuery('Ссылка удалена');
        ctx.editMessageText('Ссылка удалена');
        return;
      }
      ctx.answerCbQuery('Вы не можете удалить чужую ссылку');
    } catch (e) {
      if (e instanceof NotFoundError) {
        ctx.answerCbQuery('Ссылка не найдена');
      }
    }
  }

  async addLike(ctx) {
    try {
      const url = getUrlFromLinkMessage(ctx.update.callback_query.message.text);
      const link = await LinkService.addLike(url, ctx.update.callback_query.from.id);
      const oldLikes = +ctx.update.callback_query.message.reply_markup.inline_keyboard.flat()
                      .find((item) => item.callback_data === ADD_LIKE).text.match(/\d+/gm)[0];
      if (oldLikes === link.likes) {
        ctx.answerCbQuery('Вы уже голосовали');
        return
      }
      ctx.answerCbQuery('👍');
      ctx.editMessageText(
        ctx.update.callback_query.message.text,
        addLikesToKeyboard(link.likes)
      );
    } catch (e) {
      if (e instanceof NotFoundError) {
        ctx.answerCbQuery('Операция не выполнена');
        ctx.editMessageText('Эта ссылка была удалена');
      }
      Promise.reject(e);
    }
  }
}
