import { Markup } from 'telegraf'

export const LINKS = 'Ссылки 📚'
export const HELP = 'Помощь'
export const MAIN = 'Главная'
export const MAIN_KEYBOARD = Markup.keyboard([[LINKS], [HELP]])