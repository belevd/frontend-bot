export class NotFoundError extends Error {
  constructor (props = {}) {
    super(props);
    this.message = props.message || 'Искомый объект не найден'
  }
}