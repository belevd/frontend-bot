import 'dotenv/config';
import { Telegraf } from 'telegraf';
import { connect } from 'mongoose';
import { MainController } from './main';
import { LinkController } from './links';
import { UsersController } from './users/index.js';
import { MyContext } from './main/types';

function main(controllers) {
  const { bot } = controllers.reduce(
    (acc, controller) => new controller(acc.bot),
    {
      bot: new Telegraf<MyContext>(process.env.BOT_TOKEN),
    }
  );
  bot.launch();

  process.once('SIGINT', () => bot.stop('SIGINT'));
  process.once('SIGTERM', () => bot.stop('SIGTERM'));
}

try {
  connect(process.env.MONGODB_URL);
  main([MainController, LinkController, UsersController]);
} catch (e) {
  console.log(e);
}
