import { Context } from 'telegraf';

export interface MyContext extends Context {
  session?: {
    [k: string]: any;
  };
}
