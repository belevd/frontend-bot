import { session, Telegraf } from 'telegraf'
import {LINKS, MAIN, MAIN_KEYBOARD} from '../config/index.js'
import { LINKS_KEYBOARD } from '../links/config/index.js'

export default class MainController {
  constructor(bot) {
    this.bot = bot

    this.bot.use(session())
    this.bot.use(
      Telegraf.optional(
        async (ctx) => {
          return !ctx.session
        },
        (ctx, next) => {
          ctx.session = {}
          next()
        }
      )
    )

    this.bot.start(this.start)
    this.bot.hears(MAIN, this.goToMain)
    this.bot.hears(LINKS, this.goToLinks)
    this.bot.help((ctx) => ctx.reply('Send me a sticker'))
  }

  start(ctx) {
    ctx.reply(
      'Привет! Этот бот умеет всякое.\n' +
        'Обрати внимание на меню снизу.\n' +
        'Если возникнут какие-то вопросы, то нажми на "Помощь".',
      MAIN_KEYBOARD
    )
  }

  goToMain(ctx) {
    ctx.reply('Вы вернулись на главную', MAIN_KEYBOARD)
  }

  goToLinks(ctx) {
    ctx.reply('В этом разделе можно поработать со ссылками', LINKS_KEYBOARD)
  }
}
