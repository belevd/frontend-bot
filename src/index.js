import 'dotenv/config';
import { Telegraf } from 'telegraf';
import mongoose from 'mongoose';
import { MainController } from './main';
import { LinkController } from './links';
import { UsersController } from './users/index.js';

function main(controllers) {
  const { bot } = controllers.reduce(
    (acc, controller) => new controller(acc.bot),
    {
      bot: new Telegraf(process.env.BOT_TOKEN),
    }
  );
  bot.launch();

  process.once('SIGINT', () => bot.stop('SIGINT'));
  process.once('SIGTERM', () => bot.stop('SIGTERM'));
}

try {
  mongoose.connect(process.env.MONGODB_URL);
  main([MainController, LinkController, UsersController]);
} catch (e) {
  console.log(e);
}
